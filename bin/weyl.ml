(* SPDX-FileCopyrightText: 2022 Alois Wohlschlager <alois1@gmx-topmail.de>
 * SPDX-License-Identifier: BSD-3-Clause
 *)

let () =
  E7.words (Args.letters Sys.argv.(1)) (Args.letters Sys.argv.(2))
  |> Array.of_seq
  |> Array.iteri (fun i words ->
         Printf.printf "%d: [" i;
         match E7.WordSet.elements words with
         | [] -> ()
         | w :: ws ->
             E7.word_to_string w |> print_string;
             List.iter
               (fun word ->
                 print_string ", ";
                 E7.word_to_string word |> print_string)
               ws;
             print_string "]";
             print_newline ())
