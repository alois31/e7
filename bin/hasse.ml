(* SPDX-FileCopyrightText: 2022 Alois Wohlschlager <alois1@gmx-topmail.de>
 * SPDX-License-Identifier: BSD-3-Clause
 *)

let () =
  let enabled_letters = Args.letters Sys.argv.(1) in
  let subgroup = Args.letters Sys.argv.(2) in
  let split_at = if Array.length Sys.argv >= 4 then Args.letters Sys.argv.(3) else [] in
  print_endline "digraph hasse {\n  rankdir=LR;";
  E7.words enabled_letters subgroup
  |> Seq.iter
       (E7.WordSet.iter (fun word ->
            Printf.printf "  \"%s\";\n" (E7.word_to_string word);
            List.iter
              (fun i ->
                let neighbor = E7.push_letter i word |> E7.reduce subgroup in
                if E7.word_length word < E7.word_length neighbor && not (List.mem i split_at) then
                  Printf.printf "  \"%s\" -> \"%s\" [label=%d];\n" (E7.word_to_string word) (E7.word_to_string neighbor) i)
              enabled_letters));
  print_endline "}"
