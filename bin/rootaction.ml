(* SPDX-FileCopyrightText: 2022 Alois Wohlschlager <alois1@gmx-topmail.de>
 * SPDX-License-Identifier: BSD-3-Clause
 *)

let mirror root simple =
  match root with
  | [ x1; x2; x3; x4; x5; x6; x7 ] -> (
      match simple with
      | 1 -> [ -x1 + x3; x2; x3; x4; x5; x6; x7 ]
      | 2 -> [ x1; -x2 + x4; x3; x4; x5; x6; x7 ]
      | 3 -> [ x1; x2; x1 - x3 + x4; x4; x5; x6; x7 ]
      | 4 -> [ x1; x2; x3; x2 + x3 - x4 + x5; x5; x6; x7 ]
      | 5 -> [ x1; x2; x3; x4; x4 - x5 + x6; x6; x7 ]
      | 6 -> [ x1; x2; x3; x4; x5; x5 - x6 + x7; x7 ]
      | 7 -> [ x1; x2; x3; x4; x5; x6; x6 - x7 ]
      | _ -> failwith "invalid simple root")
  | _ -> failwith "invalid root"

let () =
  let init =
    match Sys.argv.(2) with
    | "1" -> [ 1; 0; 0; 0; 0; 0; 0 ]
    | "2" -> [ 0; 1; 0; 0; 0; 0; 0 ]
    | "3" -> [ 0; 0; 1; 0; 0; 0; 0 ]
    | "4" -> [ 0; 0; 0; 1; 0; 0; 0 ]
    | "5" -> [ 0; 0; 0; 0; 1; 0; 0 ]
    | "6" -> [ 0; 0; 0; 0; 0; 1; 0 ]
    | "7" -> [ 0; 0; 0; 0; 0; 0; 1 ]
    | _ -> failwith "invalid simple root"
  in
  List.fold_left mirror init (Args.letters Sys.argv.(1)) |> List.iter (Printf.printf "%d ");
  print_newline ()
