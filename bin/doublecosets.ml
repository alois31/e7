(* SPDX-FileCopyrightText: 2022 Alois Wohlschlager <alois1@gmx-topmail.de>
 * SPDX-License-Identifier: BSD-3-Clause
 *)

let () =
  let enabled_letters = Args.letters Sys.argv.(2) in
  let right_subgroup = Args.letters Sys.argv.(3) in
  let left_subgroup_complement = List.filter (fun letter -> not (List.mem letter (Args.letters Sys.argv.(1)))) enabled_letters in
  E7.words enabled_letters right_subgroup
  |> Seq.map
       (E7.WordSet.filter (fun word ->
            List.for_all (fun letter -> E7.word_length (E7.push_letter letter word |> E7.reduce right_subgroup) >= E7.word_length word) left_subgroup_complement))
  |> Array.of_seq
  |> Array.iteri (fun i words ->
         Printf.printf "%d: [" i;
         match E7.WordSet.elements words with
         | [] -> print_string "]\n"
         | w :: ws ->
             E7.word_to_string w |> print_string;
             List.iter
               (fun word ->
                 print_string ", ";
                 E7.word_to_string word |> print_string)
               ws;
             print_string "]";
             print_newline ())
