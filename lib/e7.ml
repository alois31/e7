(* SPDX-FileCopyrightText: 2022 Alois Wohlschlager <alois1@gmx-topmail.de>
 * SPDX-License-Identifier: BSD-3-Clause
 *)

type word = int list

module WordSet = Set.Make (struct
  type t = word

  let compare = compare
end)

let empty_word = []
let push_letter letter word = letter :: word
let word_length = List.length
let word_to_string word = List.map Int.to_string word |> String.concat ""

type shift_result = Short of word | Long of word

let rec shift letter word =
  match letter :: word with
  (* Basic rules *)
  | 1 :: 1 :: xs -> Short xs
  | 2 :: 2 :: xs -> Short xs
  | 3 :: 3 :: xs -> Short xs
  | 4 :: 4 :: xs -> Short xs
  | 5 :: 5 :: xs -> Short xs
  | 6 :: 6 :: xs -> Short xs
  | 7 :: 7 :: xs -> Short xs
  | 2 :: 1 :: xs -> shift_all [ 1; 2 ] xs
  | 3 :: 2 :: xs -> shift_all [ 2; 3 ] xs
  | 4 :: 1 :: xs -> shift_all [ 1; 4 ] xs
  | 5 :: 1 :: xs -> shift_all [ 1; 5 ] xs
  | 5 :: 2 :: xs -> shift_all [ 2; 5 ] xs
  | 5 :: 3 :: xs -> shift_all [ 3; 5 ] xs
  | 6 :: 1 :: xs -> shift_all [ 1; 6 ] xs
  | 6 :: 2 :: xs -> shift_all [ 2; 6 ] xs
  | 6 :: 3 :: xs -> shift_all [ 3; 6 ] xs
  | 6 :: 4 :: xs -> shift_all [ 4; 6 ] xs
  | 7 :: 1 :: xs -> shift_all [ 1; 7 ] xs
  | 7 :: 2 :: xs -> shift_all [ 2; 7 ] xs
  | 7 :: 3 :: xs -> shift_all [ 3; 7 ] xs
  | 7 :: 4 :: xs -> shift_all [ 4; 7 ] xs
  | 7 :: 5 :: xs -> shift_all [ 5; 7 ] xs
  | 3 :: 1 :: 3 :: xs -> shift_all [ 1; 3; 1 ] xs
  | 4 :: 2 :: 4 :: xs -> shift_all [ 2; 4; 2 ] xs
  | 4 :: 3 :: 4 :: xs -> shift_all [ 3; 4; 3 ] xs
  | 5 :: 4 :: 5 :: xs -> shift_all [ 4; 5; 4 ] xs
  | 6 :: 5 :: 6 :: xs -> shift_all [ 5; 6; 5 ] xs
  | 7 :: 6 :: 7 :: xs -> shift_all [ 6; 7; 6 ] xs
  (* Ad-hoc rules *)
  | 3 :: 1 :: 2 :: xs -> shift_all [ 2; 3; 1 ] xs
  | 4 :: 3 :: 1 :: 4 :: xs -> shift_all [ 3; 4; 3; 1 ] xs
  | 5 :: 4 :: 2 :: 5 :: xs -> shift_all [ 4; 5; 4; 2 ] xs
  | 5 :: 4 :: 3 :: 5 :: xs -> shift_all [ 4; 5; 4; 3 ] xs
  | 6 :: 5 :: 4 :: 6 :: xs -> shift_all [ 5; 6; 5; 4 ] xs
  | 7 :: 6 :: 5 :: 7 :: xs -> shift_all [ 6; 7; 6; 5 ] xs
  | 4 :: 2 :: 3 :: 4 :: 2 :: xs -> shift_all [ 3; 4; 2; 3; 4 ] xs
  | 4 :: 2 :: 3 :: 4 :: 3 :: xs -> shift_all [ 2; 4; 2; 3; 4 ] xs
  | 5 :: 4 :: 2 :: 3 :: 5 :: xs -> shift_all [ 4; 5; 4; 2; 3 ] xs
  | 5 :: 4 :: 3 :: 1 :: 5 :: xs -> shift_all [ 4; 5; 4; 3; 1 ] xs
  | 6 :: 5 :: 4 :: 2 :: 6 :: xs -> shift_all [ 5; 6; 5; 4; 2 ] xs
  | 6 :: 5 :: 4 :: 3 :: 6 :: xs -> shift_all [ 5; 6; 5; 4; 3 ] xs
  | 7 :: 6 :: 5 :: 4 :: 7 :: xs -> shift_all [ 6; 7; 6; 5; 4 ] xs
  | 4 :: 2 :: 3 :: 1 :: 4 :: 2 :: xs -> shift_all [ 3; 4; 2; 3; 1; 4 ] xs
  | 5 :: 4 :: 2 :: 3 :: 1 :: 5 :: xs -> shift_all [ 4; 5; 4; 2; 3; 1 ] xs
  | 6 :: 5 :: 4 :: 2 :: 3 :: 6 :: xs -> shift_all [ 5; 6; 5; 4; 2; 3 ] xs
  | 6 :: 5 :: 4 :: 3 :: 1 :: 6 :: xs -> shift_all [ 5; 6; 5; 4; 3; 1 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 7 :: xs -> shift_all [ 6; 7; 6; 5; 4; 2 ] xs
  | 7 :: 6 :: 5 :: 4 :: 3 :: 7 :: xs -> shift_all [ 6; 7; 6; 5; 4; 3 ] xs
  | 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 1 :: xs -> shift_all [ 2; 4; 2; 3; 1; 4; 3 ] xs
  | 5 :: 4 :: 2 :: 3 :: 4 :: 5 :: 4 :: xs -> shift_all [ 4; 5; 4; 2; 3; 4; 5 ] xs
  | 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 6 :: xs -> shift_all [ 5; 6; 5; 4; 2; 3; 1 ] xs
  | 6 :: 5 :: 4 :: 2 :: 3 :: 4 :: 6 :: xs -> shift_all [ 5; 6; 5; 4; 2; 3; 4 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 7 :: xs -> shift_all [ 6; 7; 6; 5; 4; 2; 3 ] xs
  | 7 :: 6 :: 5 :: 4 :: 3 :: 1 :: 7 :: xs -> shift_all [ 6; 7; 6; 5; 4; 3; 1 ] xs
  | 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 5 :: 4 :: xs -> shift_all [ 4; 5; 4; 2; 3; 1; 4; 5 ] xs
  | 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 6 :: xs -> shift_all [ 5; 6; 5; 4; 2; 3; 1; 4 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 7 :: xs -> shift_all [ 6; 7; 6; 5; 4; 2; 3; 1 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 4 :: 7 :: xs -> shift_all [ 6; 7; 6; 5; 4; 2; 3; 4 ] xs
  | 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 6 :: xs -> shift_all [ 5; 6; 5; 4; 2; 3; 1; 4; 3 ] xs
  | 6 :: 5 :: 4 :: 2 :: 3 :: 4 :: 5 :: 6 :: 5 :: xs -> shift_all [ 5; 6; 5; 4; 2; 3; 4; 5; 6 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 7 :: xs -> shift_all [ 6; 7; 6; 5; 4; 2; 3; 1; 4 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 4 :: 5 :: 7 :: xs -> shift_all [ 6; 7; 6; 5; 4; 2; 3; 4; 5 ] xs
  | 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 4 :: 3 :: xs -> shift_all [ 4; 5; 4; 2; 3; 1; 4; 3; 5; 4 ] xs
  | 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 5 :: 6 :: 5 :: xs -> shift_all [ 5; 6; 5; 4; 2; 3; 1; 4; 5; 6 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 5 :: 7 :: xs -> shift_all [ 6; 7; 6; 5; 4; 2; 3; 1; 4; 5 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 7 :: xs -> shift_all [ 6; 7; 6; 5; 4; 2; 3; 1; 4; 3 ] xs
  | 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 4 :: 2 :: 3 :: xs -> shift_all [ 4; 5; 4; 2; 3; 1; 4; 3; 5; 4; 2 ] xs
  | 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 6 :: 5 :: xs -> shift_all [ 5; 6; 5; 4; 2; 3; 1; 4; 3; 5; 6 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 7 :: xs -> shift_all [ 6; 7; 6; 5; 4; 2; 3; 1; 4; 3; 5 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 4 :: 5 :: 6 :: 7 :: 6 :: xs -> shift_all [ 6; 7; 6; 5; 4; 2; 3; 4; 5; 6; 7 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 4 :: 7 :: xs -> shift_all [ 6; 7; 6; 5; 4; 2; 3; 1; 4; 3; 5; 4 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 5 :: 6 :: 7 :: 6 :: xs -> shift_all [ 6; 7; 6; 5; 4; 2; 3; 1; 4; 5; 6; 7 ] xs
  | 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 4 :: 6 :: 5 :: 4 :: xs -> shift_all [ 5; 6; 5; 4; 2; 3; 1; 4; 3; 5; 4; 6; 5 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 4 :: 2 :: 7 :: xs -> shift_all [ 6; 7; 6; 5; 4; 2; 3; 1; 4; 3; 5; 4; 2 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 6 :: 7 :: 6 :: xs -> shift_all [ 6; 7; 6; 5; 4; 2; 3; 1; 4; 3; 5; 6; 7 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 4 :: 6 :: 7 :: 6 :: xs -> shift_all [ 6; 7; 6; 5; 4; 2; 3; 1; 4; 3; 5; 4; 6; 7 ] xs
  | 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 4 :: 2 :: 6 :: 5 :: 4 :: 2 :: xs -> shift_all [ 5; 6; 5; 4; 2; 3; 1; 4; 3; 5; 4; 2; 6; 5; 4 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 4 :: 2 :: 6 :: 7 :: 6 :: xs -> shift_all [ 6; 7; 6; 5; 4; 2; 3; 1; 4; 3; 5; 4; 2; 6; 7 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 4 :: 6 :: 5 :: 7 :: 6 :: 5 :: xs -> shift_all [ 6; 7; 6; 5; 4; 2; 3; 1; 4; 3; 5; 4; 6; 5; 7; 6 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 4 :: 2 :: 6 :: 5 :: 7 :: 6 :: 5 :: xs ->
      shift_all [ 6; 7; 6; 5; 4; 2; 3; 1; 4; 3; 5; 4; 2; 6; 5; 7; 6 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 4 :: 2 :: 6 :: 5 :: 4 :: 7 :: 6 :: 5 :: 4 :: xs ->
      shift_all [ 6; 7; 6; 5; 4; 2; 3; 1; 4; 3; 5; 4; 2; 6; 5; 4; 7; 6; 5 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 4 :: 2 :: 6 :: 5 :: 4 :: 3 :: 7 :: 6 :: 5 :: 4 :: 3 :: xs ->
      shift_all [ 6; 7; 6; 5; 4; 2; 3; 1; 4; 3; 5; 4; 2; 6; 5; 4; 3; 7; 6; 5; 4 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 4 :: 2 :: 6 :: 5 :: 4 :: 3 :: 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: xs ->
      shift_all [ 6; 7; 6; 5; 4; 2; 3; 1; 4; 3; 5; 4; 2; 6; 5; 4; 3; 7; 6; 5; 4; 2 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 4 :: 2 :: 6 :: 5 :: 4 :: 3 :: 1 :: 7 :: 6 :: 5 :: 4 :: 3 :: 1 :: xs ->
      shift_all [ 6; 7; 6; 5; 4; 2; 3; 1; 4; 3; 5; 4; 2; 6; 5; 4; 3; 1; 7; 6; 5; 4; 3 ] xs
  | 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: 4 :: 3 :: 5 :: 4 :: 2 :: 6 :: 5 :: 4 :: 3 :: 1 :: 7 :: 6 :: 5 :: 4 :: 2 :: 3 :: 1 :: xs ->
      shift_all [ 6; 7; 6; 5; 4; 2; 3; 1; 4; 3; 5; 4; 2; 6; 5; 4; 3; 1; 7; 6; 5; 4; 2; 3 ] xs
  | xs -> Long xs

and shift_all letters word = shift_rev_all (List.rev letters) word

and shift_rev_all letters word =
  match letters with
  | [] -> Long word
  | l :: ls -> ( match shift l word with Short w -> ( match shift_rev_all ls w with Short w -> Short w | Long w -> Short w) | Long w -> shift_rev_all ls w)

let acceptable subgroup_complement word =
  List.for_all (fun letter -> match shift_rev_all (letter :: List.rev word) [] with Short _ -> false | Long _ -> true) subgroup_complement

let expand_one word enabled_letters subgroup_complement =
  Seq.filter_map
    (fun letter -> match shift letter word with Short _ -> None | Long word -> if acceptable subgroup_complement word then Some word else None)
    enabled_letters

let expand words enabled_letters subgroup_complement =
  words |> WordSet.to_seq |> Seq.flat_map (fun word -> expand_one word enabled_letters subgroup_complement) |> WordSet.of_seq

let complement subgroup = List.filter (fun letter -> not (List.mem letter subgroup)) [ 1; 2; 3; 4; 5; 6; 7 ]

let words enabled_letters subgroup =
  let subgroup_complement = complement subgroup in
  Seq.unfold
    (fun prev -> if WordSet.is_empty prev then None else Some (prev, expand prev (List.to_seq enabled_letters) subgroup_complement))
    (WordSet.singleton [])

let simple_reductions subgroup_complement word =
  List.filter_map (fun letter -> match shift_rev_all (letter :: List.rev word) [] with Short word -> Some word | Long _ -> None) subgroup_complement

let reduce subgroup word =
  let subgroup_complement = complement subgroup in
  let rec reduce' word = match simple_reductions subgroup_complement word with [] -> word | word :: _ -> reduce' word in
  let reduce_borel word = match shift_all word [] with Short word -> word | Long word -> word in
  reduce_borel word |> reduce'
