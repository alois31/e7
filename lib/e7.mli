(* SPDX-FileCopyrightText: 2022 Alois Wohlschlager <alois1@gmx-topmail.de>
 * SPDX-License-Identifier: BSD-3-Clause
 *)

type word

module WordSet : Set.S with type elt = word

val empty_word : word
val push_letter : int -> word -> word
val word_length : word -> int
val word_to_string : word -> string

(* Generate all minimal representatives in W/W_P where given letters are enabled, and P is the given parabolic subgroup *)
val words : int list -> int list -> WordSet.t Seq.t

(* Find a minimal representative in the same class *)
val reduce : int list -> word -> word
