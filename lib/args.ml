(* SPDX-FileCopyrightText: 2022 Alois Wohlschlager <alois1@gmx-topmail.de>
 * SPDX-License-Identifier: BSD-3-Clause
 *)

let letters str = String.to_seq str |> Seq.map (fun c -> Char.code c - Char.code '0') |> List.of_seq
